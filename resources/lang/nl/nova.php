<?php

return [
    'internal_reference' => 'Interne referentie',
    'internal_reference_help' => 'Reference code for your own use. Maximum 20 characters.',

    'ean' => 'EAN',
    'ean_help' => '13 cijferig EAN/ISBN nummer. (aanvullen met voorloopnullen*)',

    'condition' => 'Conditie',
    'condition_help' => 'Conditie van het offer. Kies uit; nieuw, als nieuw, goed, redelijk, matig',

    'condition_comment' => 'Conditie commentaar',
    'condition_comment_help' => 'Toelichting op de conditie. Wordt enkel gebruikt voor offers met 2ehands conditie (als nieuw, goed, redelijk, matig). Gebruik maximaal 2000 karakters.',
    'condition_new' => 'Nieuw product, inclusief alle wettelijke garanties.',
    'condition_like_new' => 'Gebruikt, schoon en intact. Geschikt als cadeau.',
    'condition_good' => 'Prima conditie. Lichte sporen van gebruik.',
    'condition_reasonable' => 'Gebruikt maar compleet, heeft sporen van gebruik. Bijvoorbeeld lichte verkleuring, krassen.',
    'condition_mediocre' => 'Nog te gebruiken exemplaar, compleet. Verkleuring en oppervlakkige beschadigingen zijn aanwezig.',

    'stock' => 'Voorraad',
    'stock_help' => 'Voorraad die je beschikbaar hebt (wordt automatisch bijgewerkt wanneer je een artikel via bol.com verkocht heeft). Numerieke waarde maximaal 999. Wordt enkel voor afleverwijze (Fulfillment by) verkoper gebruikt.',

    'price' => 'Prijs',
    'price_help' => 'Prijs van je offer. Numerieke waarde tussen 1 en 9999,99.',

    'delivery_time' => 'Levertijd',
    'delivery_time_help' => 'Levertijd van je offer. Enkel voor offers met conditie nieuw en voor afleverwijze (Fulfillment by) verkoper.',

    'delivery_time_24hours_23' => 'Op werkdagen voor 23.00 uur besteld is volgende werkdag in huis',
    'delivery_time_24hours_22' => 'Op werkdagen voor 22.00 uur besteld is volgende werkdag in huis',
    'delivery_time_24hours_21' => 'Op werkdagen voor 21.00 uur besteld is volgende werkdag in huis',
    'delivery_time_24hours_20' => 'Op werkdagen voor 20.00 uur besteld is volgende werkdag in huis',
    'delivery_time_24hours_19' => 'Op werkdagen voor 19.00 uur besteld is volgende werkdag in huis',
    'delivery_time_24hours_18' => 'Op werkdagen voor 18.00 uur besteld is volgende werkdag in huis',
    'delivery_time_24hours_17' => 'Op werkdagen voor 17.00 uur besteld is volgende werkdag in huis',
    'delivery_time_24hours_16' => 'Op werkdagen voor 16.00 uur besteld is volgende werkdag in huis',
    'delivery_time_24hours_15' => 'Op werkdagen voor 15.00 uur besteld is volgende werkdag in huis',
    'delivery_time_24hours_14' => 'Op werkdagen voor 14.00 uur besteld is volgende werkdag in huis',
    'delivery_time_24hours_13' => 'Op werkdagen voor 13.00 uur besteld is volgende werkdag in huis',
    'delivery_time_24hours_12' => 'Op werkdagen voor 12.00 uur besteld is volgende werkdag in huis',
    'delivery_time_1_2d' => '1-2 werkdagen',
    'delivery_time_2_3d' => '2-3 werkdagen',
    'delivery_time_3_5d' => '3-5 werkdagen',
    'delivery_time_4_8d' => '4-8 werkdagen',
    'delivery_time_1_8d' => '1-8 werkdagen',
    'delivery_time_mijnleverbelofte' => 'Levertijd wordt bepaald op basis van eigen leverbelofte. Dit is momenteel enkel beschikbaar voor een selecte groep verkopers.',

    'delivery_method' => 'Afleverwijze',
    'delivery_method_help' => 'Afleverwijze van het artikel. Dit kan door de verkoper zelf zijn, of door bol.com middels de dienst Logistiek via bol.com.',
    'delivery_method_self' => 'We leveren het zelf uit',
    'delivery_method_bol' => 'Logistiek via Bol.com',

    'for_sale' => 'Te koop',
    'for_sale_help' => 'Wilt je het offer na uploaden direct te koop zetten?',

    'product_name' => 'Productnaam',
    'product_name_help' => 'Dit wordt alleen gebruikt als er een nieuw product aangemaakt wordt in Bol.com.',

    'description' => 'Beschrijving',
    'description_help' => 'Dit wordt alleen gebruikt als er een nieuw product aangemaakt wordt in Bol.com.',
];
