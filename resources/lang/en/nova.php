<?php

return [
    'internal_reference' => 'Internal reference',
    'internal_reference_help' => 'Reference code for your own use. Maximum 20 characters.',

    'ean' => 'EAN',
    'ean_help' => '13 digit EAN / ISBN number. (complete with leading zeros *)',

    'condition' => 'Condition',
    'condition_help' => 'Condition of the sacrifice. Choose; nieuw, als nieuw, goed, redelijk, matig',

    'condition_comment' => 'Condition comment',
    'condition_comment_help' => 'Explanation of the condition. Only used for offerings with second hand condition (als nieuw, goed, redelijk, matig). Use up to 2000 characters.',
    'condition_new' => 'New product, including all legal guarantees.',
    'condition_like_new' => 'Used, clean and intact. Suitable as a gift.',
    'condition_good' => 'Fine condition. Light traces of use.',
    'condition_reasonable' => 'Used but complete, has traces of use. For example, light discoloration, scratches.',
    'condition_mediocre' => 'Still to be used, complete. Discoloration and superficial damage are present.',

    'stock' => 'Stock',
    'stock_help' => 'Inventory you have available (automatically updated when you sell an item through bol.com). Numeric value up to 999. Only used for delivery method (Fulfillment by) seller.',

    'price' => 'Price',
    'price_help' => 'Price of your sacrifice. Numeric value between 1 and 9999.99.',

    'delivery_time' => 'Delivery time',
    'delivery_time_help' => 'Delivery time of your offer. Only for offers with condition new and for delivery method (Fulfillment by) seller.',
    'delivery_time_24hours_23' => 'Ordered on weekdays before 11 p.m. is delivered the next working day',
    'delivery_time_24hours_22' => 'Ordered on weekdays before 10 p.m. is delivered the next working day',
    'delivery_time_24hours_21' => 'Ordered on weekdays before 9 p.m. is delivered the next working day',
    'delivery_time_24hours_20' => 'Ordered on weekdays before 8 pm is delivered the next working day',
    'delivery_time_24hours_19' => 'Ordered on weekdays before 7 pm is delivered the next working day',
    'delivery_time_24hours_18' => 'Ordered on weekdays before 6 pm is delivered the next working day',
    'delivery_time_24hours_17' => 'Ordered on weekdays before 5 pm is delivered the next working day',
    'delivery_time_24hours_16' => 'Ordered on weekdays before 4 pm is delivered the next working day',
    'delivery_time_24hours_15' => 'Ordered on weekdays before 3 pm is delivered the next working day',
    'delivery_time_24hours_14' => 'Ordered on weekdays before 2 pm is delivered the next working day',
    'delivery_time_24hours_13' => 'Ordered on weekdays before 1 pm is delivered the next working day',
    'delivery_time_24hours_12' => 'Ordered on weekdays before noon is delivered the next working day',
    'delivery_time_1_2d' => '1-2 business days',
    'delivery_time_2_3d' => '2-3 business days',
    'delivery_time_3_5d' => '3-5 business days',
    'delivery_time_4_8d' => '4-8 business days',
    'delivery_time_1_8d' => '1-8 business days',
    'delivery_time_mijnleverbelofte' => 'Delivery time is determined based on own delivery promise. This is currently only available to a select group of sellers.',

    'delivery_method' => 'Delivery method',
    'delivery_method_help' => 'Delivery method of the article. This can be by the seller himself or by bol.com through the Logistics service via bol.com.',
    'delivery_method_self' => 'We deliver it ourselves',
    'delivery_method_bol' => 'Logistics via Bol.com',

    'for_sale' => 'For sale',
    'for_sale_help' => 'Do you want to put the offer up for sale immediately after uploading it?',

    'product_name' => 'Product name',
    'product_name_help' => 'This is only used when a new product is created in Bol.com.',

    'description' => 'Description',
    'description_help' => 'This is only used when a new product is created in Bol.com.',
];
