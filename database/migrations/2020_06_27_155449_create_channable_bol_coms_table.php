<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChannableBolComsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('channable_bol_coms', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('channable_bol_id')->unsigned();
            $table->string('channable_bol_type');
            $table->string('internal_reference');
            $table->string('ean')->nullable()->default(null);
            $table->string('condition');
            $table->text('condition_comment')->nullable()->default(null);
            $table->bigInteger('price')->nullable()->default(null);
            $table->string('delivery_time');
            $table->string('delivery_method');
            $table->boolean('for_sale');
            $table->string('product_name')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('channable_bol_id')->references('id')->on('products');
            $table->unique(['channable_bol_id', 'channable_bol_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('channable_bol_coms');
    }
}
