![alt text](https://cdn.marshmallow-office.com/media/images/logo/marshmallow.transparent.red.png "marshmallow.")

# Marshmallow Bol.com
...

### Installatie
```
composer require marshmallow/channels-bolcom
```

### Checks
 - Make sure `\App\Models\Product` extends `Marshmallow\Product\Models\Product`.
 - Make sure `App\Nova\Product` uses `public static $model = 'App\Models\Product';`.
 - Make sure `\App\Models\Product` uses `use BolCom;`.

### Usage
Add the Bol.com Channel to the product config array. This will make all the fields to your Nova resource so you can maintain your bol.com content.
```php
'channels' => [
	\Marshmallow\Channels\BolCom\Facades\BolComNovaChannel::class
	// ...
]
```

Make sure you have an observer on your product model in `EventServiceProvider.php`.
```php
public function boot()
{
    parent::boot();

	\App\Models\Product::observe(
		new \Marshmallow\Product\Observers\ProductObserver()
	);
}
```
