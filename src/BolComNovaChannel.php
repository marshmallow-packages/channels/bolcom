<?php

namespace Marshmallow\Channels\BolCom;

use Marshmallow\Priceable\Facades\Price;
use Marshmallow\Channels\Channable\Fields\Text;
use Marshmallow\Channels\Channable\Fields\Number;
use Marshmallow\Channels\Channable\Fields\Select;
use Marshmallow\Channels\Channable\Fields\Boolean;
use Marshmallow\Channels\Channable\Fields\Currency;
use Marshmallow\Channels\Channable\Fields\Textarea;
use Marshmallow\Channels\Channable\Contracts\NovaChannel;
use Marshmallow\Channels\Channable\Traits\ResourceChannable;
use Marshmallow\Channels\BolCom\Fields\Options\ConditionOptions;
use Marshmallow\Channels\BolCom\Fields\Options\DeliveryTimeOptions;
use Marshmallow\Channels\BolCom\Fields\Options\DeliveryMethodOptions;

class BolComNovaChannel implements NovaChannel
{
	use ResourceChannable;

	public function novaTabName(): string
	{
		return 'Bol.com';
	}

	public function getDefaults(): array
	{
		return config('bol.defaults');
	}

	public function morphRelationshipName(): string
	{
		return 'bolcomable';
	}

	public function translationPrefix(): string
	{
		return 'bolcom';
	}

    public function novaTabFields(): array
    {
    	return [
        	Text::make('internal_reference', $this)
        		->rules('max:20'),

        	Text::make('ean', $this)
        		->rules('max:13'),

        	Select::make('condition', $this)
        		->options(ConditionOptions::class),

        	Textarea::make('condition_comment', $this)
        		->rules('max:2000'),

        	Number::make('stock', $this)
        		->readonly()
        		->rules('max:999')
        		->fillUsing(function ($request, $model, $requestAttribute, $attribute) {
        			// Don't fill this. Stock can not be overwritten on this level.
        		}),

        	Currency::make('price', $this)
        		->rules('min:0|max:9999.99')
        		->displayUsing(function ($value, $resource, $attribute) {
        			return Price::formatAmount(
        				app('channable')->channel($this)->set($resource)->get($value, $resource, $attribute) * 100
        			);
        		}),

        	Select::make('delivery_time', $this)
        		->options(DeliveryTimeOptions::class),

        	Select::make('delivery_method', $this)
        		->options(DeliveryMethodOptions::class),

        	Boolean::make('for_sale', $this),

        	Text::make('product_name', $this)
        		->rules('max:250'),

        	Textarea::make('description', $this)
        		->rules('max:10000')
        		->resolveUsing(function ($value, $resource, $attribute) {
					return strip_tags(
						app('channable')->channel($this)->set($resource)->get($value, $resource, $attribute)
					);
				}),
        ];
    }
}
