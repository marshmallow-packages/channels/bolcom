<?php

namespace Marshmallow\Channels\BolCom\Models;

use Illuminate\Database\Eloquent\Model;

class ChannableBolCom extends Model
{
	protected $guarded = [];

	public function resource()
	{
		return $this->channable_bol_type::find($this->channable_bol_id);
	}

	public function apiPayload()
	{
		return [
			'internal_reference' => $this->internal_reference,
			'ean' => $this->ean,
			'condition' => $this->condition,
			'condition_comment' => $this->condition_comment,
			'price' => $this->price,
			'stock' => $this->resource()->freeStock(),
			'delivery_time' => $this->delivery_time,
			'delivery_method' => $this->delivery_method,
			'for_sale' => $this->for_sale,
			'product_name' => $this->product_name,
			'description' => $this->description,
		];
	}
}
