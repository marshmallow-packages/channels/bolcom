<?php

namespace Marshmallow\Channels\BolCom\Facades;

class BolComNovaChannel extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return \Marshmallow\Channels\BolCom\BolComNovaChannel::class;
    }
}
