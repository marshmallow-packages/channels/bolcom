<?php

namespace Marshmallow\Channels\BolCom\Fields\Options;

use Marshmallow\Channels\Channable\Fields\Traits\Options;

class ConditionOptions
{
	use Options;

	public function toArray()
	{
		return [
			'nieuw' => __('bolcom::nova.condition_new'),
			'als nieuw' => __('bolcom::nova.condition_like_new'),
			'goed' => __('bolcom::nova.condition_good'),
			'redelijk' => __('bolcom::nova.condition_reasonable'),
			'matig' => __('bolcom::nova.condition_mediocre'),
		];
	}
}
