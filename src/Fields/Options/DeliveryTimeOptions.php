<?php

namespace Marshmallow\Channels\BolCom\Fields\Options;

use Marshmallow\Channels\Channable\Fields\Traits\Options;

class DeliveryTimeOptions
{
	use Options;

	public function toArray()
	{
		return [
			'24uurs-23' => __('bolcom::nova.delivery_time_24hours_23'),
			'24uurs-22' => __('bolcom::nova.delivery_time_24hours_22'),
			'24uurs-21' => __('bolcom::nova.delivery_time_24hours_21'),
			'24uurs-20' => __('bolcom::nova.delivery_time_24hours_20'),
			'24uurs-19' => __('bolcom::nova.delivery_time_24hours_19'),
			'24uurs-18' => __('bolcom::nova.delivery_time_24hours_18'),
			'24uurs-17' => __('bolcom::nova.delivery_time_24hours_17'),
			'24uurs-16' => __('bolcom::nova.delivery_time_24hours_16'),
			'24uurs-15' => __('bolcom::nova.delivery_time_24hours_15'),
			'24uurs-14' => __('bolcom::nova.delivery_time_24hours_14'),
			'24uurs-13' => __('bolcom::nova.delivery_time_24hours_13'),
			'24uurs-12' => __('bolcom::nova.delivery_time_24hours_12'),

			'1-2d' => __('bolcom::nova.delivery_time_1_2d'),
			'2-3d' => __('bolcom::nova.delivery_time_2_3d'),
			'3-5d' => __('bolcom::nova.delivery_time_3_5d'),
			'4-8d' => __('bolcom::nova.delivery_time_4_8d'),
			'1-8d' => __('bolcom::nova.delivery_time_1_8d'),

			'MijnLeverbelofte' => __('bolcom::nova.delivery_time_mijnleverbelofte'),
		];
	}
}
