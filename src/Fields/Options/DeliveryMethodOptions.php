<?php

namespace Marshmallow\Channels\BolCom\Fields\Options;

use Marshmallow\Channels\Channable\Fields\Traits\Options;

class DeliveryMethodOptions
{
	use Options;

	public function toArray()
	{
		return [
			'verkoper' => __('bolcom::nova.delivery_method_self'),
			'Lvb' => __('bolcom::nova.delivery_method_bol'),
		];
	}
}
