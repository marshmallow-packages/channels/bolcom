<?php

return [
	'defaults' => [
		'internal_reference' => 'id',
		'ean' => 'ean',
		'condition' => 'redelijk',
		'stock' => 'freeStock',
		'price' => 'getPriceAttribute',
		'delivery_time' => '1-2d',
		'delivery_method' => 'verkoper',
		'product_name' => 'name',
		'description' => 'description',
		'for_sale' => true,
	]
];
